use std::fs;
use std::num::ParseIntError;
use std::str::FromStr;

struct Range {
    low: i32,
    high: i32
}

impl FromStr for Range {
    type Err = ParseIntError;
    
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let range_bounds: Vec<&str> = s.split("-").collect();
        Ok(
            Range {
                low: range_bounds[0].parse().unwrap(),
                high: range_bounds[1].parse().unwrap()
            }
        )
    }
}

fn ranges_fully_overlap(range1: &Range, range2: &Range) -> bool {
    // Range 1 contains range 2
    if range1.low <= range2.low && range1.high >= range2.high {
        return true;
    }
    // Range 2 contains range 1
    if range2.low <= range1.low && range2.high >= range1.high {
        return true;
    }
    false
}

fn ranges_overlap(range1: &Range, range2: &Range) -> bool {
    for v in range1.low..=range1.high {
        if (range2.low..=range2.high).contains(&v) {
            return true;
        }
    }
    false
}

fn part_one(elf_pairs: &Vec<&str>) {
    let mut num_overlaps = 0;
    for elf_pair in elf_pairs {
        if elf_pair.len() == 0 {
            continue;
        }
        let ranges: Vec<&str> = elf_pair.split(",").collect();
        let first_elf_range = Range::from_str(&ranges[0]).unwrap();
        let second_elf_range = Range::from_str(&ranges[1]).unwrap();
        if ranges_fully_overlap(&first_elf_range, &second_elf_range) {
            num_overlaps += 1;
        }
    }
    println!("{}", num_overlaps);
}

fn part_two(elf_pairs: &Vec<&str>) {
    let mut num_overlaps = 0;
    for elf_pair in elf_pairs {
        if elf_pair.len() == 0 {
            continue;
        }
        let ranges: Vec<&str> = elf_pair.split(",").collect();
        let first_elf_range = Range::from_str(&ranges[0]).unwrap();
        let second_elf_range = Range::from_str(&ranges[1]).unwrap();
        if ranges_overlap(&first_elf_range, &second_elf_range) {
            num_overlaps += 1;
        }
    }
    println!("{}", num_overlaps);
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");

    //let test_data = "2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8";

    let elf_pairs: Vec<&str> = data.split("\n").collect();

    part_one(&elf_pairs);
    part_two(&elf_pairs);
}
