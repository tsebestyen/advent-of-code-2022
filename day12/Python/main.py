import rustworkx as rx


class Point:
    def __init__(self, name, coords, node) -> None:
        self.name = name
        self.coords = coords
        self.node = node

    def __repr__(self) -> str:
        return f"{self.name} {self.coords}"


def debug_print(data, nodes):
    data_array = data.replace('\n', '')
    data_list = list(data_array)
    for node in nodes:
        # print(p // 144 + 1, p % 144 + 1)
        data_list[node] = data_list[node].upper()
    data_str = ''.join(data_list)
    nl = len(data_str) // 144
    for i in range(nl):
        print(data_str[i*144:i*144 + 144])


def part_one(data):
    graph, S, E, D, points = parse_graph(data)
    path = rx.dijkstra_shortest_paths(graph, S, E, weight_fn=lambda x: 1)
    # debug_print(data, path[E])
    print(len(path[E]) - 1)


def part_two(data):
    graph, S, E, D, points = parse_graph(data)
    lengths = []
    a_points = [p.node for p in points.values() if p.name == 'a']
    a_points.append(S)
    for p_idx, p in enumerate(a_points):
        # print(f"Calculating path {p_idx + 1}/{len(a_points)}")
        path = rx.dijkstra_shortest_paths(graph, p, E, weight_fn=lambda x: 1)
        if E in path:
            lengths.append(len(path[E]) - 1)
    print(sorted(lengths)[0])


def can_go_to(c, n):
    start_to_a = (c == 'S' and n == 'a')
    if start_to_a:
        return True
    z_to_end = (c == 'z' and n == 'E')
    if z_to_end:
        return True
    can_ascend = ord(n) - ord(c) in (0, 1) and c != 'S'
    if can_ascend:
        return True
    can_descend = ord(n) - ord(c) < 0 and n not in ('S', 'E')
    if can_descend:
        return True
    return False


def parse_graph(data: str):
    # We must use a directed graph
    graph = rx.PyDiGraph()

    lines = data.splitlines()

    S = None
    E = None

    # Nodes
    nodes = {}
    edges = []
    for line_idx, line in enumerate(lines):
        for c_idx, c in enumerate(line):
            name = c
            coords = (line_idx, c_idx)
            node = graph.add_node(coords)
            if c == 'S':
                S = node
            elif c == 'E':
                E = node
            nodes[coords] = Point(name, coords, node)
            # Can go left
            if c_idx > 0:
                n = line[c_idx - 1]
                if can_go_to(c, n):
                    edges.append(((line_idx, c_idx), (line_idx, c_idx - 1)))
            # Can go right
            if c_idx < len(line) - 1:
                n = line[c_idx + 1]
                if can_go_to(c, n):
                    edges.append(((line_idx, c_idx), (line_idx, c_idx + 1)))
            # Can go up
            if line_idx > 0:
                n = lines[line_idx - 1][c_idx]
                if can_go_to(c, n):
                    edges.append(((line_idx, c_idx), (line_idx - 1, c_idx)))
            # Can go down
            if line_idx < len(lines) - 1:
                n = lines[line_idx + 1][c_idx]
                if can_go_to(c, n):
                    edges.append(((line_idx, c_idx), (line_idx + 1, c_idx)))

    # Debug node to find path to
    # D = points[(9, 119)].node
    D = None

    # Edges
    for edge in edges:
        s = nodes[edge[0]]
        e = nodes[edge[1]]
        graph.add_edge(s.node, e.node, 1)

    return graph, S, E, D, nodes


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    part_one(data)
    part_two(data)
