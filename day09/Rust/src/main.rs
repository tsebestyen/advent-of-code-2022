use std::fs;
use std::collections::HashSet;

#[derive(PartialEq, Eq, Hash, Clone)]
struct Position {
    x: i32,
    y: i32
}

fn adjust_tail_position(tail: &mut Position, head: &Position) {
    match (head.x - tail.x, head.y - tail.y) {
        // Same row, column
        (2, 0) => tail.x += 1,
        (-2, 0) => tail.x -= 1,
        (0, 2) => tail.y += 1,
        (0, -2) => tail.y -= 1,
        // Diagonals
        (2, 1) => { tail.x += 1; tail.y += 1; }
        (2, -1) => { tail.x += 1; tail.y -= 1; },
        (-2, 1) => { tail.x -= 1; tail.y += 1; },
        (-2, -1) => { tail.x -= 1; tail.y -= 1; },
        // More diagonals
        (1, 2) => { tail.x += 1; tail.y += 1; },
        (1, -2) => { tail.x += 1; tail.y -= 1; },
        (-1, 2) => { tail.x -= 1; tail.y += 1; },
        (-1, -2) => { tail.x -= 1; tail.y -= 1; },
        // Part 2 diagonals
        (2, 2) => { tail.x += 1; tail.y += 1; },
        (2, -2) => { tail.x += 1; tail.y -= 1; },
        (-2, 2) => { tail.x -= 1; tail.y += 1; },
        (-2, -2) => { tail.x -= 1; tail.y -= 1; },
        // Default
        (_, _) => ()
    }
}

fn part_one(moves: &Vec<(&str, i32)>) {
    let mut head = Position {
        x: 0,
        y: 0
    };

    let mut tail = Position {
        x: 0,
        y: 0
    };

    let mut visited_positions = HashSet::new();
    visited_positions.insert(tail.clone());

    for a_move in moves {
        let change = match a_move.0 {
            "R" => (1, 0),
            "L" => (-1, 0),
            "U" => (0, -1),
            "D" => (0, 1),
            _ => panic!("Unexpected move!")
        };

        (0..a_move.1).for_each(|_| {
            head.x += change.0;
            head.y += change.1;
            adjust_tail_position(&mut tail, &head);
            visited_positions.insert(tail.clone());
        })
    }

    println!("{}", visited_positions.iter().count());

}

fn print_grid(knots: &Vec<Position>) {
    let min_x = knots.iter().map(|k| k.x).min().unwrap();
    let min_y = knots.iter().map(|k| k.y).min().unwrap();
    let max_x = knots.iter().map(|k| k.x).max().unwrap();
    let max_y = knots.iter().map(|k| k.y).max().unwrap();
    let mut grid: Vec<Vec<char>> = Vec::new();
    for _ in min_y..=max_y {
        let mut r = Vec::new();
        for _ in min_x..=max_x {
            r.push('.');
        }
        grid.push(r);
    }
    for (i, k) in knots.iter().enumerate() {
        let y = k.y - min_y;
        let x = k.x - min_x;
        let c;
        if i == 0 {
            c = 'H';
        } else {
            c = char::from_digit(i as u32, 10).unwrap();
        }
        grid[y as usize][x as usize] = c;
    }
    for r in grid {
        for c in r {
            print!("{}", c);
        }
        print!("\n");
    }
}

fn part_two(moves: &Vec<(&str, i32)>) {
    let start = Position {
        x: 0,
        y: 0
    };
    let mut knots: Vec<Position> = vec![
        // Head
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        start.clone(),
        // Tail
        start.clone()
    ];

    let mut visited_positions = HashSet::new();
    visited_positions.insert(start.clone());

    for a_move in moves {
        let change = match a_move.0 {
            "R" => (1, 0),
            "L" => (-1, 0),
            "U" => (0, -1),
            "D" => (0, 1),
            _ => panic!("Unexpected move!")
        };

        // println!("");
        // print_grid(&knots);
        // println!("");

        (0..a_move.1).for_each(|_| {
            // Move head
            knots[0].x += change.0;
            knots[0].y += change.1;
            
            // Adjust all the other knots in pairs
            let pair_indexes: Vec<(usize, usize)> = (0..knots.len() - 1).zip(1..knots.len()).collect();
            for pair_index in pair_indexes {
                let (head, tail) = knots.split_at_mut(pair_index.1);
                adjust_tail_position(&mut tail[0], &head[pair_index.0]);
                if pair_index.1 == 9 {
                    visited_positions.insert(knots[9].clone());
                }
            }
        })
    }

    println!("{}", visited_positions.iter().count());
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    //let data = fs::read_to_string("test_input").expect("Unable to read input");
    //let data = fs::read_to_string("test_input_2").expect("Unable to read input");
    let moves: Vec<(&str, i32)> = data.split("\n").filter(|l| l.len() > 0).map(|l| {
        let a_move = l.split_once(" ").unwrap();
        (a_move.0, a_move.1.parse().unwrap())
    }).collect();

    part_one(&moves);
    part_two(&moves);
}

