﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Day15
{
    public class Point: IEquatable<Point>
    {
        public int x = 0;
        public int y = 0;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Equals(Point? other)
        {
            return this.x == other.x && this.y == other.y;
        }

        public override string ToString()
        {
            return $"({this.x}, {this.y})";
        }
    }

    internal class Program
    {
        static int GetManhattanDistance(Point p1, Point p2)
        {
            return Math.Abs(p1.x - p2.x) + Math.Abs(p1.y - p2.y);
        }

        static void PartOne(List<(Point, Point)> pairs, int y)
        {
            var x_s = pairs.Select(p => p.Item1.x).Concat(pairs.Select(p => p.Item2.x)).ToList();

            // Correct possible lowest X coords by sensor distances
            foreach (var pair in pairs)
            {
                var s = pair.Item1;
                var b = pair.Item2;
                var d = GetManhattanDistance(s, b);
                x_s.Add(s.x - d);
                x_s.Add(s.x + d);
            }

            var minX = x_s.Min();
            var maxX = x_s.Max();

            var noBeaconPoints = new HashSet<Point>();
            for (int x = minX; x <= maxX; x++)
            {
                var p = new Point(x, y);
                foreach(var pair in pairs)
                {
                    var s = pair.Item1;
                    var b = pair.Item2;
                    var countIn = true;
                    if (p.Equals(s))
                    {
                        countIn = false;
                    }
                    if (p.Equals(b))
                    {
                        countIn = false;
                    }
                    var sensorRadius = GetManhattanDistance(s, b);
                    var distance = GetManhattanDistance(p, s);
                    if (distance <= sensorRadius)
                    {
                        if (countIn)
                        {
                            noBeaconPoints.Add(p);
                        }
                    }
                }
            }

            Console.WriteLine(noBeaconPoints.Count);
            //Console.ReadLine();
        }
        
        static void Main(string[] args)
        {
            //var lines = File.ReadAllLines("test_input");
            var lines = File.ReadAllLines("input");

            //var y = 10;
            var y = 2_000_000;

            Regex rx = new Regex(@".*at x=(-?\d+), y=(-?\d+):.*x=(-?\d+), y=(-?\d+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

            var pairs = new List<(Point, Point)>();
            foreach (var line in lines)
            {
                // Find matches.
                MatchCollection matches = rx.Matches(line);

                var s_x = int.Parse(matches[0].Groups[1].Value);
                var s_y = int.Parse(matches[0].Groups[2].Value);
                var b_x = int.Parse(matches[0].Groups[3].Value);
                var b_y = int.Parse(matches[0].Groups[4].Value);
                pairs.Add((new Point(s_x, s_y), new Point(b_x, b_y)));
            }

            PartOne(pairs, y);
        }
    }
}