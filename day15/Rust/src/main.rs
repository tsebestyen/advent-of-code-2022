use std::fs;
use std::collections::HashSet;
use regex::Regex;

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: i32,
    y: i32
}

fn get_manhattan_distance(p1: &Point, p2: &Point) -> i32 {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

fn part_one(pairs: &mut Vec<(Point, Point)>, y: i32) {
    let mut x_s: Vec<i32> = pairs.iter().map(|p| p.0.x).collect();
    let mut x_s_2 = pairs.iter().map(|p| p.1.x).collect();
    x_s.append(&mut x_s_2);

    // Correct possible lowest X coords by sensor distances
    for (s, b) in pairs.iter() {
        let d = get_manhattan_distance(&s, &b);
        x_s.push(s.x - d);
        x_s.push(s.x + d);
    }

    let min_x = *x_s.iter().min().unwrap();
    let max_x = *x_s.iter().max().unwrap();

    let mut no_beacon_points = HashSet::new();

    let x_range = min_x..=max_x;

    for (i, x) in x_range.enumerate() {
        let p = Point {
            x: x,
            y: y
        };
        for pair in &mut *pairs {
            let (s, b) = pair;
            let mut count_in = true;
            if p == *s {
                count_in = false;
            }
            if p == *b {
                count_in = false;
            }
            let sensor_radius = get_manhattan_distance(s, b);
            let distance = get_manhattan_distance(&p, s);

            if distance <= sensor_radius {
                if count_in {
                    no_beacon_points.insert(p);
                }
            }
        }
    }

    println!("{}", no_beacon_points.len());
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");

    let y = 2_000_000;

    let re = Regex::new(r".*at x=(-?\d+), y=(-?\d+):.*x=(-?\d+), y=(-?\d+)").unwrap();

    let mut pairs = Vec::new();
    data.split("\n").for_each(|l| {
        if l.len() > 0 {
            let captures = re.captures(l).unwrap();
            let s = Point {
                x: captures.get(1).unwrap().as_str().parse().unwrap(),
                y: captures.get(2).unwrap().as_str().parse().unwrap()
            };
            let b = Point {
                x: captures.get(3).unwrap().as_str().parse().unwrap(),
                y: captures.get(4).unwrap().as_str().parse().unwrap()
            };
            pairs.push((s, b));
        }   
    });

    part_one(&mut pairs, y);
}
