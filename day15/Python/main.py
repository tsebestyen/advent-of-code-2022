import re

class Point:
    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y

    def __eq__(self, other) -> bool:
        return self.x == other.x and self.y == other.y

    def __hash__(self) -> int:
        return hash((self.x, self.y))

    def __repr__(self) -> str:
        return f"({self.x}, {self.y})"


def get_manhattan_distance(p1: Point, p2: Point) -> int:
    return abs(p1.x - p2.x) + abs(p1.y - p2.y)


def part_one(pairs, y):
    x_s = [p[0].x for p in pairs] + [p[1].x for p in pairs]
    
    # Correct possible lowest X coords by sensor distances
    for s, b in pairs:
        d = get_manhattan_distance(s, b)
        x_s.append(s.x - d)
        x_s.append(s.x + d)
    
    min_x = min(x_s)
    max_x = max(x_s)

    debug_row = list('.' * (max_x - min_x + 1))

    no_beacon_points = set()
    for i, x in enumerate(range(min_x, max_x + 1)):
        p = Point(x, y)
        for pair in pairs:
            s, b = pair[0], pair[1]
            count_in = True
            if s == p:
                debug_row[i] = 'S'
                count_in = False
            if b == p:
                debug_row[i] = 'B'
                count_in = False
            sensor_radius = get_manhattan_distance(s, b)
            distance = get_manhattan_distance(p, s)
            if distance <= sensor_radius:
                if count_in:
                    debug_row[i] = '#'
                    no_beacon_points.add(p)

    # print(''.join(debug_row))
    print(len(no_beacon_points))


def part_two(pairs):
    min_x = 0
    # max_x = 20
    max_x = 4_000_000
    min_y = 0
    # max_y = 20
    max_y = 4_000_000

    sensor_ranges = {}
    for s, b in pairs:
        d = get_manhattan_distance(s, b)
        for i in range(d + 1):
            # Up
            x_s = s.x - d + i
            x_e = s.x + d - i
            y_u = s.y - i
            y_d = s.y + i
            if x_s <= min_x:
                x_s = min_x
            if x_e > max_x:
                x_e = max_x
            if s.y == y_u:
                if y_u not in sensor_ranges:
                    sensor_ranges[y_u] = []
                sensor_ranges[y_u].append((x_s, x_e))
                continue
            if min_y <= y_u <= max_y:
                if y_u not in sensor_ranges:
                    sensor_ranges[y_u] = []
                sensor_ranges[y_u].append((x_s, x_e))
            if min_y <= y_d <= max_y:
                if y_d not in sensor_ranges:
                    sensor_ranges[y_d] = []
                sensor_ranges[y_d].append((x_s, x_e))

    # Find row with not fully overlapping ranges
    for y, ranges in sensor_ranges.items():
        sorted_ranges = sorted(ranges)
        row_range = []
        for r in sorted_ranges:
            if not row_range:
                row_range.append(r[0])
                row_range.append(r[1])
                continue
            if r[0] < row_range[0]:
                row_range[0] = r[0]
            if r[0] <= row_range[1] + 1 and r[1] > row_range[1]:
                row_range[1] = r[1]
        if row_range[1] - row_range[0] != max_x - min_x:
            break
    
    x = row_range[1] + 1
    print(x * 4_000_000 + y)


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.readlines()
    
    # y = 10
    y = 2_000_000

    pairs = []
    for line in data:
        match = re.match(".*at x=(-?\d+), y=(-?\d+):.*x=(-?\d+), y=(-?\d+)", line)
        s_x = int(match.group(1))
        s_y = int(match.group(2))
        b_x = int(match.group(3))
        b_y = int(match.group(4))
        pairs.append((Point(s_x, s_y), Point(b_x, b_y)))
    
    # part_one(pairs, y)
    part_two(pairs)
