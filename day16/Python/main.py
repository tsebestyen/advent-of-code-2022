import re
from copy import deepcopy
from itertools import permutations, repeat
from typing import List, Dict
import multiprocessing as mp
import rustworkx as rx
from rustworkx.visualization import mpl_draw
import matplotlib.pyplot as plt


DEBUG_PRINT_ENABLED = False

class Valve:
    def __init__(self, name, flow_rate, can_go_to) -> None:
        self.name = name
        self.is_open = False
        self.flow_rate = flow_rate
        self.can_go_to = can_go_to

    def __repr__(self) -> str:
        return self.name


def debug_print(s='', print_enabled=DEBUG_PRINT_ENABLED):
    if print_enabled:
        print(s)


def calc(tp: List[Valve], valves: List[Valve], nodes: Dict[str, Valve], graph, minutes=30):
    t_idx = 0
    pressure = 0
    minute = 1
    path_to_walk = []
    wv = deepcopy(valves)
    # Next time read the f@ck!ng instructions... 😖
    # current_valve = wv[0]
    current_valve = find_valve_by_name('AA', wv)
    v = None
    valves_opened = []
    while minute <= minutes:
        debug_print()
        debug_print(f"== Minute {minute} ==")
        open_valves = [v for v in wv if v.is_open]
        closed_valves = [v for v in wv if not v.is_open]
        debug_print(f"Open valves: {open_valves}")
        released_pressure = 0
        for ov in open_valves:
            pressure += ov.flow_rate
            released_pressure += ov.flow_rate
        if open_valves:
            debug_print(f"Pressure: {released_pressure}")
            debug_print(f"Total pressure: {pressure}")
        
        if path_to_walk:
            n = path_to_walk.pop()
            v = wv[n]
            debug_print(f"Move to valve {v.name}")
            current_valve = v
            minute += 1
            continue

        # Open the valve
        if not path_to_walk and v:
            if v.flow_rate:
                debug_print(f"Opening valve {v.name}")
                v.is_open = True
                valves_opened.append(v)
            minute += 1
            v = None
            continue

        if not path_to_walk and not v and t_idx < len(tp):
            s = nodes[current_valve.name]
            t = nodes[tp[t_idx].name]
            t_idx += 1
            shortest_path = rx.dijkstra_shortest_paths(graph, s, t, weight_fn=lambda x: 1)
            path_to_walk = shortest_path[t][1:][::-1]

            if len(path_to_walk) < minutes - minute:
                # Move
                n = path_to_walk.pop()
                v = wv[n]
                debug_print(f"Move to valve {v.name}")
                current_valve = v
                minute += 1
                continue
            else:
                path_to_walk = []
                t_idx = len(tp)
        
        if not path_to_walk and not v and t_idx == len(tp):
            # We opened all the big ones, we probably have few steps left, open the closest ones
            s = nodes[current_valve.name]
            paths = []
            for cv in closed_valves:
                t = nodes[cv.name]
                shortest_path = rx.dijkstra_shortest_paths(graph, s, t, weight_fn=lambda x: 1)
                if t not in shortest_path:
                    continue
                distance = len(shortest_path[t])
                
                d = {"d": distance, "p": shortest_path[t]}
                paths.append(d)
            
            closest_path = sorted(paths, key=lambda d: d['d'])
            path_to_walk = closest_path[0]['p'][1:][::-1]

            # Move
            n = path_to_walk.pop()
            v = wv[n]
            debug_print(f"Move to valve {v.name}")
            current_valve = v
            minute += 1
            continue

        minute += 1
    return pressure, valves_opened, tp


def find_valve_by_name(valve, valves) -> Valve:
    return next((v for v in valves if v.name == valve))


def part_one(valves: List[Valve], nodes: Dict[str, Valve], graph):
    target_valves = [v for v in valves if v.flow_rate > 0]
    # # Filter out targets that are too far from start anyway
    # s = nodes[valves[0].name]
    # t_idx = 0
    # while t_idx < len(target_valves):
    #     t = nodes[target_valves[t_idx].name]
    #     shortest_path = rx.dijkstra_shortest_paths(graph, s, t, weight_fn=lambda x: 1)
    #     path_to_walk = shortest_path[t]
    #     if len(path_to_walk) > 10:
    #         del target_valves[t_idx]
    #     else:
    #         t_idx += 1
    # target_valves = sorted(target_valves, key=lambda v: v.flow_rate, reverse=True)
    # print(target_valves)

    # start_valves = (find_valve_by_name('LS', valves),)
    start_valves = (find_valve_by_name('KQ', valves),)
    # start_valves = (find_valve_by_name('AI', valves),)
    # start_valves = (find_valve_by_name('QK', valves),)
    # start_valves = (find_valve_by_name('RF', valves),)
    # target_valves = [
    #     find_valve_by_name('LS', valves),
    #     find_valve_by_name('RF', valves),
    #     find_valve_by_name('KZ', valves),
    #     # find_valve_by_name('QK', valves),
    #     find_valve_by_name('ZJ', valves),
    #     find_valve_by_name('YC', valves),
    #     find_valve_by_name('KQ', valves),
    #     # find_valve_by_name('AI', valves),
    #     find_valve_by_name('HC', valves),
    #     find_valve_by_name('AZ', valves),
    # ]
    target_valves = [
        # find_valve_by_name('KQ', valves),
        find_valve_by_name('RF', valves),
        find_valve_by_name('AZ', valves),
        find_valve_by_name('VI', valves),
        find_valve_by_name('IY', valves),
        find_valve_by_name('HA', valves),
        find_valve_by_name('AQ', valves),
        find_valve_by_name('IM', valves),
        find_valve_by_name('KZ', valves)
    ]

    # target_permutations = permutations(start_valves + target_valves)
    target_permutations = permutations(target_valves)
    target_permutations = [start_valves + tp for tp in target_permutations]

    pressures = set()
    # pressure = calc(target_valves, valves, nodes, graph)
    with mp.Pool(7) as p:
        pressures = p.starmap(calc, zip(target_permutations, repeat(valves), repeat(nodes), repeat(graph)))
    ps = sorted(pressures, key=lambda p: p[0], reverse=True)
    print(ps[0][0])


def part_two(valves: List[Valve], nodes: Dict[str, Valve], graph):
    target_valves = [v for v in valves if v.flow_rate > 0]

    part_two_time = 26

    # Me
    start_valves = (find_valve_by_name('KQ', valves),)
    target_valves = [
        find_valve_by_name('RF', valves),
        find_valve_by_name('AZ', valves),
        find_valve_by_name('VI', valves),
        find_valve_by_name('IY', valves),
        find_valve_by_name('HA', valves),
        find_valve_by_name('AQ', valves),
        find_valve_by_name('IM', valves),
        find_valve_by_name('KZ', valves)
    ]

    target_permutations = permutations(target_valves)
    target_permutations = [start_valves + tp for tp in target_permutations]

    pressures = set()
    # pressure = calc(target_valves, valves, nodes, graph)
    with mp.Pool(7) as p:
        pressures = p.starmap(calc, zip(target_permutations, repeat(valves), repeat(nodes), repeat(graph), repeat(part_two_time)))
    ps = sorted(pressures, key=lambda p: p[0], reverse=True)
    my_pressure = ps[0][0]

    # Elephant
    start_valves = (find_valve_by_name('QK', valves),)
    target_valves = [
        find_valve_by_name('LS', valves),
        find_valve_by_name('KZ', valves),
        # find_valve_by_name('QK', valves),
        find_valve_by_name('ZJ', valves),
        find_valve_by_name('YC', valves),
        find_valve_by_name('AI', valves),
        find_valve_by_name('HC', valves),
        find_valve_by_name('AZ', valves),
    ]

    # target_permutations = permutations(start_valves + target_valves)
    target_permutations = permutations(target_valves)
    target_permutations = [start_valves + tp for tp in target_permutations]

    pressures = set()
    # pressure = calc(target_valves, valves, nodes, graph)
    with mp.Pool(7) as p:
        pressures = p.starmap(calc, zip(target_permutations, repeat(valves), repeat(nodes), repeat(graph), repeat(part_two_time)))
    ps = sorted(pressures, key=lambda p: p[0], reverse=True)
    elephant_pressure = ps[0][0]
    
    print(my_pressure + elephant_pressure)


def parse_graph(valves: List[Valve]):
    graph = rx.PyDiGraph()
    nodes = {}
    for valve in valves:
        node = graph.add_node(valve.name)
        nodes[valve.name] = node
    for valve in valves:
        s = nodes[valve.name]
        for t_name in valve.can_go_to:
            t = nodes[t_name]
            graph.add_edge(s, t, 1)
    return nodes, graph


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.readlines()
    valves = []
    for line in data:
        match = re.match(r"Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? (.*)", line)
        if match:
            can_go_to = [v.strip() for v in match.group(3).split(',')]
            v = Valve(match.group(1), int(match.group(2)), can_go_to)
            valves.append(v)
    nodes, graph = parse_graph(valves)
    node_colors = ['#428af5' if v.flow_rate > 0 else '#f54248' for v in valves]
    node_colors[0] = '#42f581'

    # mpl_draw(graph, with_labels=True, labels=lambda n: f"{n} {valves[nodes[n]].flow_rate}" if valves[nodes[n]].flow_rate else f"{n}", node_color=node_colors)
    # plt.draw()
    # plt.show()

    # part_one(valves, nodes, graph)
    part_two(valves, nodes, graph)
