import re
import functools


def compare_pairs(l, r):
    # print(f"Compare {l} vs {r}")
    if isinstance(l, int) and isinstance(r, int) and l > r:
        # print(f"Left is larger: NOT in order")
        return False
    if isinstance(l, int) and isinstance(r, int) and l < r:
        # print(f"Left is smaller: in order")
        return True
    if isinstance(l, int) and isinstance(r, list):
        l = [l]
        # print(f"Mixed types, convert left to {l} and retry")
        return compare_pairs(l, r)
    if isinstance(l, list) and isinstance(r, int):
        r = [r]
        # print(f"Mixed types, convert right to {r} and retry")
        return compare_pairs(l, r)
    if isinstance(l, list) and isinstance(r, list):
        for lv, rv in zip(l, r):
            c = compare_pairs(lv, rv)
            if c is not None:
                return c
        if len(l) < len(r):
            # print(f"Left side ran out of items")
            return True
        elif len(r) < len(l):
            # print(f"Right side ran out of items")
            return False


def compare_pairs_num(l, r):
    # print(f"Compare {l} vs {r}")
    if isinstance(l, int) and isinstance(r, int) and l > r:
        # print(f"Left is larger: NOT in order")
        return 1
    if isinstance(l, int) and isinstance(r, int) and l < r:
        # print(f"Left is smaller: in order")
        return -1
    if isinstance(l, int) and isinstance(r, list):
        l = [l]
        # print(f"Mixed types, convert left to {l} and retry")
        return compare_pairs_num(l, r)
    if isinstance(l, list) and isinstance(r, int):
        r = [r]
        # print(f"Mixed types, convert right to {r} and retry")
        return compare_pairs_num(l, r)
    if isinstance(l, list) and isinstance(r, list):
        for lv, rv in zip(l, r):
            c = compare_pairs_num(lv, rv)
            if c is not None:
                return c
        if len(l) < len(r):
            # print(f"Left side ran out of items")
            return -1
        elif len(r) < len(l):
            # print(f"Right side ran out of items")
            return 1


def part_one(pairs):
    pair_idx_in_order = []
    for pair_idx, pair in enumerate(pairs, start=1):
        if compare_pairs(pair[0], pair[1]):
            pair_idx_in_order.append(pair_idx)
    print(sum(pair_idx_in_order))


def part_two(pairs):
    # packets = []
    # for pair in pairs:
    #     left_str = str(pair[0])
    #     right_str = str(pair[1])
    #     left_digits = ','.join([f"{d:0>2}" for d in re.findall('\d+', left_str)])
    #     right_digits = ','.join([f"{d:0>2}" for d in re.findall('\d+', right_str)])
    #     packets.append(left_digits)
    #     packets.append(right_digits)
    # packets.append('02')
    # packets.append('06')

    packets = [pair[0] for pair in pairs] + [pair[1] for pair in pairs]
    packets.append([[2]])
    packets.append([[6]])

    cmp = functools.cmp_to_key(compare_pairs_num)

    # ordered_packets = sorted(packets)
    ordered_packets = sorted(packets, key=cmp)
    idx_1 = ordered_packets.index([[2]]) + 1
    idx_2 = ordered_packets.index([[6]]) + 1
    print(idx_1 * idx_2)


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()

    pairs_str = data.split("\n\n")
    pairs = [(eval(l), eval(r)) for l,r in [p.split('\n', maxsplit=1) for p in pairs_str]]
    part_one(pairs)
    part_two(pairs)
