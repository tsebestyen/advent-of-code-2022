from typing import List, Dict


class Monkey:
    def __init__(self) -> None:
        self.monkey_id: int = 0
        self.items: List[int] = []
        self.operation_type: str = ''
        self.operation_value: str = ''
        self.test: int = 0
        self.monkey_true: int = 0
        self.monkey_false: int = 0
        self.items_inspected: int = 0

    def __repr__(self) -> str:
        return f"Monkey {self.monkey_id}"


class Monkey_v2:
    def __init__(self, monkey: Monkey, divisors: List[int]) -> None:
        self.monkey_id: int = monkey.monkey_id
        self.item_remainders: List[Dict[int, int]] = []
        for item in monkey.items:
            remainders = {}
            for d in divisors:
                remainders[d] = item % d
            self.item_remainders.append(remainders)
        self.operation_type: str = monkey.operation_type
        self.operation_value: str = monkey.operation_value
        self.test: int = monkey.test
        self.monkey_true: int = monkey.monkey_true
        self.monkey_false: int = monkey.monkey_false
        self.items_inspected: int = 0

    def __repr__(self) -> str:
        return f"Monkey v2 {self.monkey_id}"


def part_one(monkeys: List[Monkey]) -> None:
    for round in range(20):
        for monkey in monkeys:
            for item in monkey.items:
                match monkey.operation_value:
                    case "old": operand = item
                    case _: operand = int(monkey.operation_value)
                match monkey.operation_type:
                    case '+': new_worry = item + operand
                    case '*': new_worry = item * operand
                new_worry //= 3
                if new_worry % monkey.test == 0:
                    monkey_to_pass = next((m for m in monkeys if m.monkey_id == monkey.monkey_true))
                else:
                    monkey_to_pass = next((m for m in monkeys if m.monkey_id == monkey.monkey_false))
                monkey_to_pass.items.append(new_worry)
                monkey.items_inspected += 1;
            monkey.items.clear()
        
        # print(f"Round {round}:")
        # for monkey in monkeys:
        #     items = ', '.join(map(str, monkey.items))
        #     print(f"Monkey {monkey.monkey_id}: {items}")

    active_monkeys = sorted(monkeys, key=lambda m: m.items_inspected, reverse=True)
    print(active_monkeys[0].items_inspected * active_monkeys[1].items_inspected)


def part_two(monkeys: List[Monkey_v2]) -> None:
    for round in range(10_000):
        for monkey in monkeys:
            for remainders in monkey.item_remainders:
                for d, r in remainders.items():
                    match monkey.operation_value:
                        case "old": operand = r
                        case _: operand = int(monkey.operation_value)
                    match monkey.operation_type:
                        case '+': remainders[d] = (r + operand) % d
                        case '*': remainders[d] = (r * operand) % d

                if remainders[monkey.test] == 0:
                    monkey_to_pass = next((m for m in monkeys if m.monkey_id == monkey.monkey_true))
                else:
                    monkey_to_pass = next((m for m in monkeys if m.monkey_id == monkey.monkey_false))
                monkey_to_pass.item_remainders.append(remainders)
                monkey.items_inspected += 1
            monkey.item_remainders.clear()

        # if round + 1 in (20, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000):
        #     print(f"Round {round + 1}:")
        #     for monkey in monkeys:
        #         print(f"Monkey {monkey.monkey_id}: {monkey.items_inspected}")
        #     print()

    active_monkeys = sorted(monkeys, key=lambda m: m.items_inspected, reverse=True)
    print(active_monkeys[0].items_inspected * active_monkeys[1].items_inspected)


def parse_monkeys(data: str) -> List[Monkey]:
    monkeys = []
    for line in data.splitlines():
        if line.startswith("Monkey"):
            monkey_idx = int(line[7])
        elif line.startswith("  Starting"):
            starting_items = list(map(int, line[18:].split(",")))
        elif line.startswith("  Operation"):
            operation_type, operation_value = line[23:].split(" ", 1)
        elif line.startswith("  Test"):
            test = int(line[21:])
        elif line.startswith("    If true"):
            monkey_true = int(line[29:])
        elif line.startswith("    If false"):
            monkey_false = int(line[30:])
            monkey = Monkey()
            monkey.monkey_id = monkey_idx
            monkey.items = starting_items
            monkey.operation_type = operation_type
            monkey.operation_value = operation_value
            monkey.test = test
            monkey.monkey_true = monkey_true
            monkey.monkey_false = monkey_false
            monkey.items_inspected = 0
            monkeys.append(monkey)
    return monkeys


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    monkeys = parse_monkeys(data)
    part_one(monkeys)
    
    monkeys = parse_monkeys(data)
    monkey_divisors = [monkey.test for monkey in monkeys]
    monkeys_v2 = [Monkey_v2(monkey, monkey_divisors) for monkey in monkeys]
    part_two(monkeys_v2)
