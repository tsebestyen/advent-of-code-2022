use std::fs;
use std::str::FromStr;

const LOSE_SCORE: usize = 0;
const DRAW_SCORE: usize = 3;
const WIN_SCORE: usize = 6;
const ROCK_SCORE: usize = 1;
const PAPER_SCORE: usize = 2;
const SCISSOR_SCORE: usize = 3;

#[derive(PartialEq)]
enum Symbol {
    Rock,
    Paper,
    Scissor
}

enum Outcome {
    Win,
    Lose,
    Draw
}

impl FromStr for Symbol {
    type Err = ();

    fn from_str(input: &str) -> Result<Symbol, Self::Err> {
        match input {
            "A"  => Ok(Symbol::Rock),
            "B"  => Ok(Symbol::Paper),
            "C"  => Ok(Symbol::Scissor),
            "X" => Ok(Symbol::Rock),
            "Y" => Ok(Symbol::Paper),
            "Z" => Ok(Symbol::Scissor),
            _      => Err(()),
        }
    }
}

impl FromStr for Outcome {
    type Err = ();

    fn from_str(input: &str) -> Result<Outcome, Self::Err> {
        match input {
            "X" => Ok(Outcome::Lose),
            "Y" => Ok(Outcome::Draw),
            "Z" => Ok(Outcome::Win),
            _      => Err(()),
        }
    }
}

fn score_for_my_choice(choice: &Symbol) -> usize {
    match choice {
        Symbol::Rock => return ROCK_SCORE,
        Symbol::Paper => return PAPER_SCORE,
        Symbol::Scissor => return SCISSOR_SCORE
    }
}

fn symbol_for_outcome(opponent_choice: &Symbol, outcome: &Outcome) -> Symbol {
    match (opponent_choice, outcome) {
        (Symbol::Rock, Outcome::Lose) => return Symbol::Scissor,
        (Symbol::Rock, Outcome::Draw) => return Symbol::Rock,
        (Symbol::Rock, Outcome::Win) => return Symbol::Paper,
        (Symbol::Paper, Outcome::Lose) => return Symbol::Rock,
        (Symbol::Paper, Outcome::Draw) => return Symbol::Paper,
        (Symbol::Paper, Outcome::Win) => return Symbol::Scissor,
        (Symbol::Scissor, Outcome::Lose) => return Symbol::Paper,
        (Symbol::Scissor, Outcome::Draw) => return Symbol::Scissor,
        (Symbol::Scissor, Outcome::Win) => return Symbol::Rock
    }
}


fn part_one(rounds: &Vec<&str>) {
    let mut score = 0;
    for round in rounds {
        if round.len() == 0 {
            continue;
        }
        let choices: Vec<&str> = round.split(" ").collect();
        let opponent_choice = Symbol::from_str(choices[0]).unwrap();
        let my_choice = Symbol::from_str(choices[1]).unwrap();

        let is_draw = opponent_choice == my_choice;
        if is_draw {
            score += DRAW_SCORE + score_for_my_choice(&my_choice);
        } else {
            score += match (&opponent_choice, &my_choice) {
                (Symbol::Rock, Symbol::Paper) => WIN_SCORE,
                (Symbol::Rock, Symbol::Scissor) => LOSE_SCORE,
                (Symbol::Paper, Symbol::Rock) => LOSE_SCORE,
                (Symbol::Paper, Symbol::Scissor) => WIN_SCORE,
                (Symbol::Scissor, Symbol::Rock) => WIN_SCORE,
                (Symbol::Scissor, Symbol::Paper) => LOSE_SCORE,
                (_, _) => 0
            };
            score += score_for_my_choice(&my_choice);
        }
    }
    println!("{}", score);
}

fn part_two(rounds: &Vec<&str>) {
    let mut score = 0;
    for round in rounds {
        if round.len() == 0 {
            continue;
        }
        let choices: Vec<&str> = round.split(" ").collect();
        let opponent_choice = Symbol::from_str(choices[0]).unwrap();
        let outcome = Outcome::from_str(choices[1]).unwrap();

        let my_choice = symbol_for_outcome(&opponent_choice, &outcome);

        let is_draw = opponent_choice == my_choice;
        if is_draw {
            score += DRAW_SCORE + score_for_my_choice(&my_choice);
        } else {
            score += match (&opponent_choice, &my_choice) {
                (Symbol::Rock, Symbol::Paper) => WIN_SCORE,
                (Symbol::Rock, Symbol::Scissor) => LOSE_SCORE,
                (Symbol::Paper, Symbol::Rock) => LOSE_SCORE,
                (Symbol::Paper, Symbol::Scissor) => WIN_SCORE,
                (Symbol::Scissor, Symbol::Rock) => WIN_SCORE,
                (Symbol::Scissor, Symbol::Paper) => LOSE_SCORE,
                (_, _) => 0
            };
            score += score_for_my_choice(&my_choice);
        }
    }
    println!("{}", score);
}

fn main() {
    let contents = fs::read_to_string("input")
        .expect("Should have been able to read the file");

    let rounds: Vec<&str> = contents.split("\n").collect();

    //let rounds = vec!["A Y","B X","C Z"];

    part_one(&rounds);
    part_two(&rounds);

    println!("Hello, world!");
}
