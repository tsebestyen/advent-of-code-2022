use std::fs;
use std::collections::HashSet;

fn part_one(rucksacks: &Vec<&str>) {
    let mut priorities: Vec<u32> = Vec::new();

    for rucksack in rucksacks {
        if rucksack.len() == 0 {
            break;
        }
        let set1: HashSet<char> = HashSet::from_iter(rucksack[..rucksack.len()/2].chars());
        let set2: HashSet<char> = HashSet::from_iter(rucksack[rucksack.len()/2..].chars());

        let shared_item: &char = set1.intersection(&set2).next().unwrap();

        let priority = match shared_item {
            'a'..='z' => *shared_item as u32 - 96,
            'A'..='Z' => *shared_item as u32 - 64 + 26,
            _ => panic!("Unexpected item")
        };

        priorities.push(priority);
    }

    let sum_priorities: u32 = priorities.iter().sum();

    println!("{}", sum_priorities);
}

fn part_two(rucksacks: &Vec<&str>) {
    let mut priorities: Vec<u32> = Vec::new();

    let groups = rucksacks.chunks(3);

    for group in groups {
        if group.len() < 3 {
            break;
        }
        let mut set1: HashSet<char> = HashSet::from_iter(group[0].chars());
        let set2: HashSet<char> = HashSet::from_iter(group[1].chars());
        let set3: HashSet<char> = HashSet::from_iter(group[2].chars());

        set1.retain(|c| set2.contains(c));
        set1.retain(|c| set3.contains(c));
        let shared_item = set1.iter().next().unwrap();

        let priority = match shared_item {
            'a'..='z' => *shared_item as u32 - 96,
            'A'..='Z' => *shared_item as u32 - 64 + 26,
            _ => panic!("Unexpected item")
        };

        priorities.push(priority);
    }

    let sum_priorities: u32 = priorities.iter().sum();

    println!("{}", sum_priorities);
}

fn main() {
    let data = fs::read_to_string("input").expect("Could not read input");

    //let test_data = "vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw";

    let rucksacks: Vec<&str> = data.split("\n").collect();
    
    part_one(&rucksacks);
    part_two(&rucksacks);
}
