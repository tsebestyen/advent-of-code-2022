use std::fs;

fn is_tree_visible(forest: &Vec<Vec<u32>>, row_idx: usize, col_idx: usize) -> bool {
    let current_tree_height = forest[row_idx][col_idx];
    let is_visible_from_left: bool = !forest[row_idx][0..col_idx].iter().any(|t| t >= &current_tree_height);
    let is_visible_from_top: bool = !forest[0..row_idx].iter().map(|r| r[col_idx]).any(|t| t >= current_tree_height);
    let is_visible_from_right: bool = !forest[row_idx][col_idx + 1..forest[row_idx].len()].iter().any(|t| t >= &current_tree_height);
    let is_visible_from_bottom: bool = !forest[row_idx + 1..forest.len()].iter().map(|r| r[col_idx]).any(|t| t >= current_tree_height);
    is_visible_from_left || is_visible_from_top || is_visible_from_right || is_visible_from_bottom
}

fn part_one(forest: &Vec<Vec<u32>>) {
    let num_cols = forest[0].len();
    let num_rows = forest.len();
    let edges = 2 * num_rows + 2 * num_cols - 4;
    
    let mut num_visible = 0;
    for row_idx in 1..num_rows - 1 {
        for col_idx in 1..num_cols - 1 {
            if is_tree_visible(&forest, row_idx, col_idx) {
                num_visible += 1;
            }
        }
    }
    
    println!("{}", edges + num_visible);
}

fn calc_scenic_score(forest: &Vec<Vec<u32>>, row_idx: usize, col_idx: usize) -> usize {
    let current_tree_height = forest[row_idx][col_idx];
    let mut left_trees = 0;
    for i in (0..col_idx).rev() {
        if forest[row_idx][i] <= current_tree_height {
            left_trees += 1;
        } else {
            left_trees += 1;
            break;
        }
        if forest[row_idx][i] == current_tree_height {
            break;
        }
    }
    let mut top_trees = 0;
    for i in (0..row_idx).rev() {
        if forest[i][col_idx] <= current_tree_height {
            top_trees += 1;
        } else {
            top_trees += 1;
            break;
        }
        if forest[i][col_idx] == current_tree_height {
            break;
        }
    }
    let mut right_trees = 0;
    for i in col_idx + 1..forest[row_idx].len() {
        if forest[row_idx][i] <= current_tree_height {
            right_trees += 1;
        } else {
            right_trees += 1;
            break;
        }
        if forest[row_idx][i] == current_tree_height {
            break;
        }
    }
    let mut bottom_trees = 0;
    for i in row_idx + 1..forest.len() {
        if forest[i][col_idx] <= current_tree_height {
            bottom_trees += 1;
        } else {
            bottom_trees += 1;
            break;
        }
        if forest[i][col_idx] == current_tree_height {
            break;
        }
    }
    
    left_trees * top_trees * right_trees * bottom_trees
}

fn part_two(forest: &Vec<Vec<u32>>) {
    let num_cols = forest[0].len();
    let num_rows = forest.len();

    let mut scores: Vec<usize> = Vec::new();
    for row_idx in 1..num_rows - 1 {
        for col_idx in 1..num_cols - 1 {
            let score = calc_scenic_score(&forest, row_idx, col_idx);
            scores.push(score);
        }
    }    
    
    println!("{}", scores.iter().max().unwrap());
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    //let test_data = fs::read_to_string("test_input").expect("Unable to read input");

    let forest: Vec<Vec<u32>> = data.split("\n").filter(|l| l.len() > 0).map(|r| r.chars().map(|d| d.to_digit(10).unwrap()).collect()).collect();

    part_one(&forest);
    part_two(&forest);
}
