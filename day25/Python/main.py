def to_snafu(n: int) -> str:
    retval = ''
    p = 0
    while True:
        v = pow(5, p)
        if n < 2 * v:
            break
        p += 1
    while p > -1:
        v = pow(5, p)

        if n > v:
            d = 2
        elif n > 0:
            d = 1
        elif n < -v:
            d = -2
        elif n < 0:
            d = -1
        else:
            d = 0
        
        max_remaining = 0
        for i in range(p - 1, -1, -1):
            max_remaining += 2 * pow(5, i)
        
        r = n - v * d

        if max_remaining < abs(r):
            d += 1 if n < 0 else -1

        if d >= 0:
            retval += str(d)
        elif d == -1:
            retval += '-'
        elif d == -2:
            retval += '='
        
        p -= 1
        n -= v * d

    return retval


def part_one(data):
    snafus = []
    for snafu in data:
        num = 0
        for d_idx, d in enumerate(snafu[::-1]):
            match d:
                case '-': v = -1
                case '=': v = -2
                case _: v = int(d)
            num += pow(5, d_idx) * v
        snafus.append(num)
    print(to_snafu(sum(snafus)))


def part_two(data):
    pass


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read().splitlines()

    part_one(data)
    part_two(data)
