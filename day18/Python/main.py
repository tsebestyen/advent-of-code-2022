from typing import List


class Cube:
    def __init__(self, x, y, z) -> None:
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other: object) -> bool:
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __repr__(self) -> str:
        return f"({self.x}, {self.y}, {self.z})"


def get_covered_faces(cube: Cube, cubes: List[Cube]) -> int:
    num_neighbours = 0
    for other_cube in cubes:
        if cube == other_cube:
            continue
        # X
        if other_cube.x == cube.x - 1 and other_cube.y == cube.y and other_cube.z == cube.z:
            num_neighbours += 1
        if other_cube.x == cube.x + 1 and other_cube.y == cube.y and other_cube.z == cube.z:
            num_neighbours += 1
        # Y
        if other_cube.x == cube.x and other_cube.y - 1 == cube.y and other_cube.z == cube.z:
            num_neighbours += 1
        if other_cube.x == cube.x and other_cube.y + 1 == cube.y and other_cube.z == cube.z:
            num_neighbours += 1
        # Z
        if other_cube.x == cube.x and other_cube.y == cube.y and other_cube.z - 1 == cube.z:
            num_neighbours += 1
        if other_cube.x == cube.x and other_cube.y == cube.y and other_cube.z + 1 == cube.z:
            num_neighbours += 1
    return num_neighbours


def part_one(cubes: List[Cube]):
    visible_faces = 0
    for cube in cubes:
        f = 6 - get_covered_faces(cube, cubes)
        visible_faces += f
    
    print(visible_faces)


def part_two(cubes: List[Cube]):
    pass


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()

    cubes = []
    for line in data.splitlines():
        tokens = line.split(',')
        x, y, z = int(tokens[0]), int(tokens[1]), int(tokens[2])
        cubes.append(Cube(x, y, z))
    
    part_one(cubes)
    # part_two(cubes)
