import rustworkx as rx
from rustworkx.visualization import mpl_draw
import matplotlib.pyplot as plt
from sympy import symbols, solve


class Monkey:
    def __init__(self, name, number=0, operation='') -> None:
        self.name = name
        self.number = number
        self.operation = operation

    def __repr__(self) -> str:
        return self.name


def build_graph(monkeys):
    # Build dependency graph
    graph = rx.PyDiGraph()
    nodes = {}
    for monkey in monkeys:
        node = graph.add_node(monkey.name)
        nodes[monkey.name] = node

    for monkey in monkeys:
        if monkey.operation:
            s = nodes[monkey.name]
            tokens = monkey.operation.split()
            dep1 = tokens[0]
            dep1_node = nodes[dep1]
            dep2 = tokens[2]
            dep2_node = nodes[dep2]
            graph.add_edge(s, dep1_node, 1)
            graph.add_edge(s, dep2_node, 1)
    
    # mpl_draw(graph, with_labels=True, labels=str)
    # plt.draw()
    # plt.show()
    return nodes, graph


def part_one(monkeys):
    nodes, graph = build_graph(monkeys)

    top_sort = list(reversed(rx.topological_sort(graph)))
    monkey_numbers = {}
    for ni in top_sort:
        m = monkeys[ni]
        if m.number:
            monkey_numbers[m.name] = m.number
        else:
            m.number = eval(m.operation, {}, monkey_numbers)
            monkey_numbers[m.name] = m.number
    
    print(monkey_numbers['root'])


def part_two(monkeys):
    nodes, graph = build_graph(monkeys)
    root = nodes['root']
    humn = nodes['humn']

    shortest_path = rx.dijkstra_shortest_paths(graph, root, humn, weight_fn=lambda x: 1)
    
    # # Remove the path to human so the other branch gets calculated
    # for nid in shortest_path[humn]:
    #     if nid != root:
    #         graph.remove_node(nid)

    # top_sort = list(reversed(rx.topological_sort(graph)))
    # monkey_numbers = {}
    # for ni in top_sort:
    #     m = monkeys[ni]
    #     if m.number:
    #         monkey_numbers[m.name] = m.number
    #     else:
    #         m.number = eval(m.operation, {}, monkey_numbers)
    #         monkey_numbers[m.name] = m.number
    
    # # Root is mcnw + wqdw and wqdw = 31343426392931.0
    # # -> Human branch be also this number

    x = symbols('x')

    top_sort = list(reversed(rx.topological_sort(graph)))
    monkey_expressions = {}
    for ni in top_sort:
        # if ni not in shortest_path_node_ids:
        #     continue
        m = monkeys[ni]
        if m.name == 'humn':
            monkey_expressions[m.name] = 'x'
        elif m.number:
            monkey_expressions[m.name] = m.number
        else:
            tokens = m.operation.split()
            
            op1 = tokens[0]
            op2 = tokens[2]

            exp1 = monkey_expressions[op1]
            exp2 = monkey_expressions[op2]

            m_expression = f"({exp1}) {tokens[1]} ({exp2})"

            monkey_expressions[m.name] = m_expression

    root = next((m for m in monkeys if m.name == 'root'))
    exp = root.operation.split()[0]
    expression = monkey_expressions[exp]

    r = solve(f'31343426392931 - {expression}', x)

    print(r)
  

if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    monkeys = []
    for line in data.splitlines():
        name, operation = line.split(':', maxsplit=1)
        operation = operation.strip()
        if operation.isdecimal():
            number = int(operation)
            m = Monkey(name, number=number)
        else:
            m = Monkey(name, operation=operation)
        monkeys.append(m)

    # part_one(monkeys)
    part_two(monkeys)
