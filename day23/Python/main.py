from typing import List, Set


class Coord:
    def __init__(self, x, y) -> None:
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return f"{self.x}, {self.y}"

    def __eq__(self, o: object) -> bool:
        return self.x == o.x and self.y == o.y

    def __hash__(self) -> int:
        return hash((self.x, self.y))


STEPS = ('N', 'S', 'W', 'E')


def get_map(elves: List[Coord]):
    min_x = min([elf.x for elf in elves])
    min_y = min([elf.y for elf in elves])
    max_x = max([elf.x for elf in elves])
    max_y = max([elf.y for elf in elves])
    map_str = ''
    for y in range(min_y, max_y + 1):
        row = ''
        for x in range(min_x, max_x + 1):
            if Coord(x, y) not in elves:
                row += '.'
            else:
                row += '#'
        row += '\n'
        map_str += row
    return map_str


def get_coord(elf: Coord, direction: str) -> Coord:
    match direction:
        case 'N': return Coord(elf.x, elf.y - 1)
        case 'S': return Coord(elf.x, elf.y + 1)
        case 'W': return Coord(elf.x - 1, elf.y)
        case 'E': return Coord(elf.x + 1, elf.y)
        case 'NE': return Coord(elf.x + 1, elf.y - 1)
        case 'NW': return Coord(elf.x - 1, elf.y - 1)
        case 'SE': return Coord(elf.x + 1, elf.y + 1)
        case 'SW': return Coord(elf.x - 1, elf.y + 1)


def get_nearby_elves(elf: Coord, direction: str) -> Set[Coord]:
    match direction:
        case 'N': return {get_coord(elf, 'N'), get_coord(elf, 'NE'), get_coord(elf, 'NW')}
        case 'S': return {get_coord(elf, 'S'), get_coord(elf, 'SE'), get_coord(elf, 'SW')}
        case 'W': return {get_coord(elf, 'W'), get_coord(elf, 'NW'), get_coord(elf, 'SW')}
        case 'E': return {get_coord(elf, 'E'), get_coord(elf, 'NE'), get_coord(elf, 'SE')}


def part_one(elves: List[Coord]):
    start_step_idx = 0
    for i in range(10):
        # print_map(elves)
        proposed_coords = {}
        w_elves = list(elves)
        s_elves = set(elves)
        # Propose move
        for elf in w_elves:
            adjacent_coords = set()
            adjacent_coords.add(Coord(elf.x - 1, elf.y - 1))
            adjacent_coords.add(Coord(elf.x, elf.y - 1))
            adjacent_coords.add(Coord(elf.x + 1, elf.y - 1))
            adjacent_coords.add(Coord(elf.x - 1, elf.y))
            adjacent_coords.add(Coord(elf.x + 1, elf.y))
            adjacent_coords.add(Coord(elf.x - 1, elf.y + 1))
            adjacent_coords.add(Coord(elf.x, elf.y + 1))
            adjacent_coords.add(Coord(elf.x + 1, elf.y + 1))
            # No other elves nearby
            if not s_elves & adjacent_coords:
                continue
            
            new_coord = None
            for s in range(len(STEPS)):
                step_idx = (start_step_idx + s) % len(STEPS)
                nearby_elves = get_nearby_elves(elf, STEPS[step_idx])
                if nearby_elves and s_elves & nearby_elves:
                    continue
                new_coord = get_coord(elf, STEPS[step_idx])
                proposed_coords[elf] = new_coord
                break

        moves = {}
        for elf, move in proposed_coords.items():
            num_proposition = sum([1 for v in proposed_coords.values() if v == move])
            if num_proposition > 1:
                continue
            moves[elf] = move
        
        for elf, move in moves.items():
            elves.remove(elf)
            elves.append(move)

        start_step_idx = (start_step_idx + 1) % len(STEPS)
    
    elf_map = get_map(elves)
    print(elf_map.count('.'))


def part_two(elves: List[Coord]):
    start_step_idx = 0
    round_num = 1
    while True:
        # print_map(elves)
        proposed_coords = {}
        w_elves = list(elves)
        s_elves = set(elves)
        # Propose move
        for elf in w_elves:
            adjacent_coords = set()
            adjacent_coords.add(Coord(elf.x - 1, elf.y - 1))
            adjacent_coords.add(Coord(elf.x, elf.y - 1))
            adjacent_coords.add(Coord(elf.x + 1, elf.y - 1))
            adjacent_coords.add(Coord(elf.x - 1, elf.y))
            adjacent_coords.add(Coord(elf.x + 1, elf.y))
            adjacent_coords.add(Coord(elf.x - 1, elf.y + 1))
            adjacent_coords.add(Coord(elf.x, elf.y + 1))
            adjacent_coords.add(Coord(elf.x + 1, elf.y + 1))
            # No other elves nearby
            if not s_elves & adjacent_coords:
                continue
            
            new_coord = None
            for s in range(len(STEPS)):
                step_idx = (start_step_idx + s) % len(STEPS)
                nearby_elves = get_nearby_elves(elf, STEPS[step_idx])
                if nearby_elves and s_elves & nearby_elves:
                    continue
                new_coord = get_coord(elf, STEPS[step_idx])
                proposed_coords[elf] = new_coord
                break

        if not proposed_coords:
            break

        moves = {}
        for elf, move in proposed_coords.items():
            num_proposition = sum([1 for v in proposed_coords.values() if v == move])
            if num_proposition > 1:
                continue
            moves[elf] = move
        
        for elf, move in moves.items():
            elves.remove(elf)
            elves.append(move)

        start_step_idx = (start_step_idx + 1) % len(STEPS)
        round_num += 1
    
    print(round_num)


if __name__ == '__main__':
    # with open('test_input') as f:
    # with open('test_input2') as f:
    with open('input') as f:
        data = f.read()

    elves = []
    for line_idx, line in enumerate(data.splitlines()):
        for c_idx, c in enumerate(line):
            if c == '#':
                elves.append(Coord(c_idx, line_idx))
    
    # part_one(elves)
    part_two(elves)
