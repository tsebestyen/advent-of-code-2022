use std::fs;

fn part_one(instructions: &Vec<&str>) {
    let mut instruction_stack: Vec<&str> = instructions.iter().rev().map(|i| *i).collect();
    
    let mut cpu: Vec<&str> = Vec::new();

    let mut x: i32 = 1;
    let mut cycle: i32 = 1;

    let interesting_cycles = vec![20, 60, 100, 140, 180, 220];
    let mut strengths: Vec<i32> = Vec::new();

    while instruction_stack.len() > 0 {
        if interesting_cycles.contains(&cycle) {
            strengths.push(cycle * x);
        }
        if cpu.len() > 0 {
            let (_, v) = cpu.pop().unwrap().split_once(" ").unwrap();
            x += v.parse::<i32>().unwrap();
        } else {
            let instruction = instruction_stack.pop().unwrap();
            if instruction.starts_with("addx") {
                cpu.push(instruction);
            }
        }
        cycle += 1;
    }

    let sum_strengths: i32 = strengths.iter().sum();

    println!("{}", sum_strengths);
}

fn part_two(instructions: &Vec<&str>) {
    let mut instruction_stack: Vec<&str> = instructions.iter().rev().map(|i| *i).collect();
    
    let mut cpu: Vec<&str> = Vec::new();

    let mut x: i32 = 1;
    let mut cycle: i32 = 1;

    let mut display: Vec<Vec<char>> = Vec::new();
    for _ in 0..6 {
        display.push(vec!['.'; 40]);
    }

    while instruction_stack.len() > 0 {
        let row_idx = (cycle - 1) / 40;
        let col_idx = (cycle - 1) % 40;
        
        if (x - 1..x + 2).contains(&col_idx) {
            display[row_idx as usize][col_idx as usize] = '#';
        }

        if cpu.len() > 0 {
            let (_, v) = cpu.pop().unwrap().split_once(" ").unwrap();
            x += v.parse::<i32>().unwrap();
        } else {
            let instruction = instruction_stack.pop().unwrap();
            if instruction.starts_with("addx") {
                cpu.push(instruction);
            }
        }
        cycle += 1;
    }

    for row in display {
        for c in row {
            print!("{}", c);
        }
        print!("\n");
    }
}

fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");
    //let data = fs::read_to_string("test_input").expect("Unable to read input");
    let instructions: Vec<&str> = data.split("\n").filter(|l| l.len() > 0).collect();
    part_one(&instructions);
    part_two(&instructions);
}
