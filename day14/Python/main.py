

def print_cave(cave):
    for r in cave:
        for c in r:
            print(c, end='')
        print()


def debug_print_cave(cave, sand_source, new_sand):
    cave_copy = [row[:] for row in cave]
    cave_copy[0][sand_source] = 'x'
    cave_copy[new_sand[1]][new_sand[0]] = '*'
    cave_str = '\n'.join([f'{row_idx:0>2} ' + ''.join(row) for row_idx, row in enumerate(cave_copy)])
    return cave_str


def create_cave(rocks):
    x_coords = set()
    y_coords = set()
    rock_ranges = []
    for rock in rocks:
        segments = rock.split('->')
        ranges = []
        for segment in segments:
            values = segment.split(',')
            x = int(values[0])
            y = int(values[1])
            r = (x, y)
            ranges.append(r)
            x_coords.add(x)
            y_coords.add(y)
        rock_ranges.append(ranges)
    min_x = min(x_coords)
    max_x = max(x_coords)
    min_y = 0
    max_y = max(y_coords)
    
    # Create the map
    cave = []
    for _ in range(max_y + 1):
        row = []
        for _ in range(max_x - min_x + 1):
            row.append('.')
        cave.append(row)
    
    # Place rocks on the map
    for rock_range in rock_ranges:
        for start, end in zip(rock_range, rock_range[1:]):
            # Vertical
            if start[0] == end[0]:
                x = start[0] - min_x
                y = start[1]
                d = 1 if start[1] - end[1] < 0 else -1
                for r in range(abs(start[1] - end[1]) + 1):
                    cave[y + r * d][x] = '#'
            # Horizontal
            if start[1] == end[1]:
                x = start[0] - min_x
                y = start[1]
                d = 1 if start[0] - end[0] < 0 else -1
                for r in range(abs(start[0] - end[0]) + 1):
                    cave[y][x + r * d] = '#'

    return cave, min_x, min_y, max_x, max_y


def part_one(cave, min_x):
    sand_source = 500 - min_x
    sand_units_in_rest = 0
    start_new_sand = True
    while True:
        if start_new_sand:
            new_sand = [sand_source, 0]
            start_new_sand = False
        can_move = False
        # We are in the last row and "in air"
        if new_sand[1] == len(cave) - 1 and cave[new_sand[1]][new_sand[0]] == '.':
            break
        # We are in the one to last row and there's air below us
        if new_sand[1] == len(cave) - 2 and cave[new_sand[1] + 1][new_sand[0]] == '.':
            break
        # We are at the left edge and below us not air -> we would overflow on the left
        if new_sand[0] == 0 and cave[new_sand[1] + 1][new_sand[0]] != '.':
            break

        if cave[new_sand[1] + 1][new_sand[0]] == '.':
            new_sand[1] += 1
            can_move = True
        elif cave[new_sand[1] + 1][new_sand[0] - 1] == '.':
            new_sand[0] -= 1
            new_sand[1] += 1
            can_move = True
        elif cave[new_sand[1] + 1][new_sand[0] + 1] == '.':
            new_sand[0] += 1
            new_sand[1] += 1
            can_move = True
        if not can_move:
            cave[new_sand[1]][new_sand[0]] = 'o'
            sand_units_in_rest += 1
            start_new_sand = True

    print(sand_units_in_rest)


def add_cave_row(cave, char='.'):
    num_cols = len(cave[0])
    cave.append(list(char * num_cols))


def extend_cave(cave):
    for row in cave:
        row.insert(0, '.')
        row.append('.')
    cave[-1][0] = '#'
    cave[-1][-1] = '#'


def part_two(cave, min_x):
    add_cave_row(cave)
    add_cave_row(cave, '#')

    sand_units_in_rest = 0
    start_new_sand = True
    while True:
        if start_new_sand:
            sand_source = 500 - min_x
            new_sand = [sand_source, 0]
            start_new_sand = False
        can_move = False

        # cave_debug = debug_print_cave(cave, sand_source, new_sand)

        if new_sand[0] == 0 and new_sand[1] != len(cave) - 2:
            extend_cave(cave)
            new_sand[0] += 1
            min_x -= 1
        if new_sand[0] == len(cave[0]) - 1 and new_sand[1] != len(cave) - 1:
            extend_cave(cave)
            new_sand[0] += 1
            min_x -= 1

        # cave_debug = debug_print_cave(cave, sand_source, new_sand)

        if cave[new_sand[1] + 1][new_sand[0]] == '.':
            new_sand[1] += 1
            can_move = True
        elif cave[new_sand[1] + 1][new_sand[0] - 1] == '.':
            new_sand[0] -= 1
            new_sand[1] += 1
            can_move = True
        elif cave[new_sand[1] + 1][new_sand[0] + 1] == '.':
            new_sand[0] += 1
            new_sand[1] += 1
            can_move = True
        if not can_move:
            cave[new_sand[1]][new_sand[0]] = 'o'
            sand_units_in_rest += 1
            start_new_sand = True
            if new_sand == [sand_source, 0]:
                break

    print(sand_units_in_rest)


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    rocks = data.splitlines()

    cave, min_x, min_y, max_x, max_y = create_cave(rocks)
    # print_cave(cave)

    part_one(cave, min_x)
    
    cave, min_x, min_y, max_x, max_y = create_cave(rocks)
    part_two(cave, min_x)
