use std::fs;
use regex::Regex;
use chrono::Utc;

const TEST_NUM_STACKS: i32 = 3;
const TEST_NUM_STACK_LINES: i32 = 3;
const TEST_LINES_TO_SKIP: i32 = 5;
const PROD_NUM_STACKS: i32 = 9;
//const PROD_NUM_STACK_LINES: i32 = 8;
const PROD_NUM_STACK_LINES: i32 = 99999;
//const PROD_LINES_TO_SKIP: i32 = 10;
const PROD_LINES_TO_SKIP: i32 = 100001;
const CHARS_BETWEEN_COLS: i32 = 4;

fn part_one(stacks: &Vec<Vec<char>>, moves: &Vec<(i32, i32, i32)>) {
    let mut my_stacks = stacks.clone();

    //let mut move_num = 1;
    for a_move in moves {
        //println!("{} Processing move {}", Utc::now().format("%Y-%m-%d %H:%M:%S.%f").to_string(), move_num);
        for _ in 0..a_move.0 {
            let item = my_stacks[a_move.1 as usize - 1].pop().unwrap();
            my_stacks[a_move.2 as usize - 1].push(item);
        }
        //move_num += 1;
    }
    
    let results: Vec<char> = my_stacks.iter().map(|s| *s.last().unwrap()).collect();

    let s: String = results.into_iter().collect();
    println!("{}", s);
}

fn part_two(stacks: &Vec<Vec<char>>, moves: &Vec<(i32, i32, i32)>) {
    let mut my_stacks = stacks.clone();

    let mut temp_stack: Vec<char> = Vec::new();

    for a_move in moves {
        for _ in 0..a_move.0 {
            let item = my_stacks[a_move.1 as usize - 1].pop().unwrap();
            temp_stack.push(item);
        }

        for _ in 0..a_move.0 {
            let item = temp_stack.pop().unwrap();
            my_stacks[a_move.2 as usize - 1].push(item);
        }
    }

    let results: Vec<char> = my_stacks.iter().map(|s| *s.last().unwrap()).collect();

    let s: String = results.into_iter().collect();
    println!("{}", s);
}

fn parse_stacks(data: &str) -> Vec<Vec<char>> {
    let mut stacks = Vec::new();
    for _ in 0..PROD_NUM_STACKS {
        stacks.push(Vec::new());
    }

    let stack_lines: Vec<&str> = data.split("\n").take(PROD_NUM_STACK_LINES as usize).collect();

    for stack_line in stack_lines {
        for col_idx in 0..PROD_NUM_STACKS {
            let col_position = 1 + col_idx * CHARS_BETWEEN_COLS;
            let stack_item = stack_line.chars().nth(col_position as usize).unwrap();
            if stack_item != ' ' {
                stacks[col_idx as usize].push(stack_item);
            }
        }
    }

    for i in 0..stacks.len() {
        stacks[i] = stacks[i].iter().rev().map(|c| *c).collect();
    }

    stacks
}

fn parse_moves(data: &str) -> Vec<(i32, i32, i32)> {
    let move_lines: Vec<&str> = data.split("\n").skip(PROD_LINES_TO_SKIP as usize).collect();
    let re = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();

    let mut moves: Vec<(i32, i32, i32)> = Vec::new();
    for a_move in move_lines {
        if a_move.len() == 0 {
            continue;
        }
        let captures = re.captures(a_move).unwrap();

        moves.push(
            (captures.get(1).unwrap().as_str().parse().unwrap(), 
            captures.get(2).unwrap().as_str().parse().unwrap(), 
            captures.get(3).unwrap().as_str().parse().unwrap())
        );
    }
    moves
}

fn main() {
    //let data = fs::read_to_string("input").expect("Unable to read input");
    let data = fs::read_to_string("aoc_2022_day05_large_input.txt").expect("Unable to read input");
    //let test_data = fs::read_to_string("test_input").expect("Unable to read input");

    let stacks = parse_stacks(&data);
    //println!("{} Stacks parsed", Utc::now().format("%Y-%m-%d %H:%M:%S.%f").to_string());
    let moves = parse_moves(&data);
    //println!("{} Moves parsed", Utc::now().format("%Y-%m-%d %H:%M:%S.%f").to_string());

    part_one(&stacks, &moves);
    part_two(&stacks, &moves);
}
