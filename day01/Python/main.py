def part_one(elves):
    return max([sum([int(calorie) for calorie in elf.split("\n") if calorie]) for elf in elves])


def part_two(elves):
    return sum(sorted([sum([int(calorie) for calorie in elf.split("\n") if calorie]) for elf in elves], reverse=True)[:3])


if __name__ == '__main__':
    with open('input') as f:
        data = f.read()
    elves = data.split("\n\n")
    print(part_one(elves))
    print(part_two(elves))
