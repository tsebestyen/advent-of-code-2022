use std::fs;

fn part_one(elves: &Vec<&str>) {
    let max_value: u32 = elves
        .iter()
        .map(|elf| {
            elf
                .split("\n")
                .filter(|value| value.len() > 0)
                .map(|calorie| calorie.parse::<u32>().unwrap())
                .sum()
        })
        .max().unwrap();

    println!("{}", max_value);
}

fn part_two(elves: &Vec<&str>) {
    let all_values: Vec<u32> = elves
        .iter()
        .map(|elf| {
            elf
                .split("\n")
                .filter(|value| value.len() > 0)
                .map(|calorie| calorie.parse::<u32>().unwrap())
                .sum()
        })
        .collect();

    let mut sorted_values = all_values.clone();
    sorted_values.sort();

    let sum_of_top_three: u32 = sorted_values.iter().rev().take(3).sum();

    println!("{}", sum_of_top_three);
}

fn main() {
    let contents = fs::read_to_string("input")
        .expect("Should have been able to read the file");
    let elves: Vec<&str> = contents.split("\n\n").collect();
    
    part_one(&elves);
    part_two(&elves);
}
