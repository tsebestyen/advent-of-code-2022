from typing import List

TOTAL_SIZE = 70_000_000
SPACE_REQUIRED = 30_000_000


class File:
    def __init__(self, name, size) -> None:
        self.name = name
        self.size = size

    def __repr__(self) -> str:
        return f'{self.name} (file, size={self.size})'


class Directory:
    def __init__(self, name, parent=None) -> None:
        self.name = name
        self.parent = parent
        self.items = []
        self.size = 0

    def __repr__(self) -> str:
        return f'{self.name} (dir)'


def calc_size(dir: Directory) -> int:
    size = 0
    subdirs = [item for item in dir.items if isinstance(item, Directory)]
    if subdirs:
        for subdir in subdirs:
            subdir_size = calc_size(subdir)
            subdir.size = subdir_size
            size += subdir_size
    for item in dir.items:
        if isinstance(item, File):
            size += item.size
    return size


def sum_sizes(dir: Directory) -> int:
    size = 0
    for item in dir.items:
        if isinstance(item, Directory):
            if item.size <= 100_000:
                size += item.size
            size += sum_sizes(item)
    return size


def parse_filesystem(root: Directory, output_lines: List[str]) -> None:
    current_directory = root
    for line in output_lines:
        if line.startswith('$'):
            tokens = line.split()
            match tokens[1]:
                case 'cd':
                    target = tokens[2]
                    if target == '/':
                        current_directory = root
                    elif target == '..' and current_directory.parent:
                        current_directory = current_directory.parent
                    else:
                        for i in current_directory.items:
                            if isinstance(i, Directory) and i.name == target:
                                d = i
                        current_directory = d
                case 'ls': pass
        else:
            tokens = line.split()
            if tokens[0] == 'dir':
                d = Directory(tokens[1], parent=current_directory)
                current_directory.items.append(d)
            else:
                size = int(tokens[0])
                name = tokens[1]
                f = File(name, size)
                current_directory.items.append(f)
    
    root_size = calc_size(root)
    root.size = root_size


def list_all_dirs(dir: Directory, dir_list: List[Directory]) -> None:
    for item in dir.items:
        if isinstance(item, Directory):
            dir_list.append(item)
            list_all_dirs(item, dir_list)


def part_one(root: Directory):
    sum_of_dirs_lt_10000 = sum_sizes(root)
    return sum_of_dirs_lt_10000


def part_two(root: Directory):
    unused_space = TOTAL_SIZE - root.size
    required_space = SPACE_REQUIRED - unused_space

    all_dirs: List[Directory] = [root]
    list_all_dirs(root, all_dirs)
    candidates = sorted((dir for dir in all_dirs if dir.size >= required_space), key=lambda d: d.size)

    return candidates[0].size


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    
    root = Directory('/')
    output_lines = data.splitlines()
    
    parse_filesystem(root, output_lines)
    
    print(part_one(root))
    print(part_two(root))
