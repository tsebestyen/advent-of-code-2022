use std::fs;
use itertools::Itertools;

const PART_ONE_LENGTH: usize = 4;
const PART_TWO_LENGTH: usize = 14;

fn part_one(data: &str) {
    let idx = data.chars().collect::<Vec<char>>()
                .windows(PART_ONE_LENGTH)
                .map(|w| w.iter().map(|c| *c).unique().collect::<Vec<char>>()).collect::<Vec<Vec<char>>>()
                .iter().enumerate().filter(|e| e.1.len() == PART_ONE_LENGTH)
                .next().unwrap().0 + PART_ONE_LENGTH;

    println!("{}", idx);
}

fn part_two(data: &str) {
    let idx = data.chars().collect::<Vec<char>>()
                .windows(PART_TWO_LENGTH)
                .map(|w| w.iter().map(|c| *c).unique().collect::<Vec<char>>()).collect::<Vec<Vec<char>>>()
                .iter().enumerate().filter(|e| e.1.len() == PART_TWO_LENGTH)
                .next().unwrap().0 + PART_TWO_LENGTH;

    println!("{}", idx);
}


fn main() {
    let data = fs::read_to_string("input").expect("Unable to read input");

    //let data = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";

    part_one(&data);
    part_two(&data);
}
