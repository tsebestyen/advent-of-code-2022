import os
from typing import List
from copy import deepcopy
from time import sleep


CHAMBER_WIDTH = 7

PRINT_ENABLED = False
PRINT_LIMIT = 30

shapes = [
    "####",             # -
    " # \n###\n # ",    # +
    "  #\n  #\n###",    # _|
    "#\n#\n#\n#",       # |
    "##\n##"            # O
]


def add_rows(chamber: List, n):
    for _ in range(n):
        chamber.append(list('.' * CHAMBER_WIDTH))


def print_chamber(chamber: List, shape, shape_coords, print_enabled=PRINT_ENABLED, print_limit=PRINT_LIMIT):
    if not print_enabled:
        return
    
    os.system('clear')
    
    chamber_copy = deepcopy(chamber)
    
    if shape:
        for r_idx, row in enumerate(shape.split('\n')):
            for c_idx, c in enumerate(row):
                y = len(chamber) - 1 - (shape_coords[1] + r_idx)
                x = c_idx + shape_coords[0]
                if c == '#':
                    chamber_copy[y][x] = '@'

    for row_idx, row in enumerate(chamber_copy[::1]):
        print(''.join(row))
        if print_limit and row_idx + 1 == print_limit:
            break
    print()
    # sleep(0.3)


def can_fall_down(chamber, shape, shape_coords) -> bool:
    bottom_y = len(chamber) - 1
    shape_rows = shape.split('\n')
    num_shape_rows = len(shape_rows)
    # Shape reached bottom
    if shape_coords[1] + num_shape_rows - 1 == bottom_y:
        return False
    # Collision detection
    if detect_collision(chamber, shape, (shape_coords[0], shape_coords[1] + 1)):
        return False
    return True


def rest_rock(chamber, shape, shape_coords):
    for r_idx, row in enumerate(shape.split('\n')):
        for c_idx, c in enumerate(row):
            y = len(chamber) - 1 - (shape_coords[1] + r_idx)
            x = c_idx + shape_coords[0]
            if c == '#':
                chamber[y][x] = '#'


def detect_collision(chamber, shape, new_shape_coords) -> bool:
    shape_rows = shape.split('\n')
    # Collision detection
    for r_idx, row in enumerate(shape_rows):
        y = len(chamber) - 1 - (new_shape_coords[1] + r_idx)
        for c_idx, c in enumerate(row):
            x = new_shape_coords[0] + c_idx
            if chamber[y][x] == '#' and c == '#':
                return True
    return False


def part_one(instructions):
    chamber = []
    new_rock = True
    shape_idx = 0
    instruction_idx = 0
    num_rocks_rest = 0
    prev_rocks_rest = 0
    prev_height = 0
    num_shape_cycles = 0
    num_instr_cycles = 0
    while num_rocks_rest < 2022:
        # if num_rocks_rest > 2020:
        #     global PRINT_ENABLED
        #     PRINT_ENABLED = True
        
        # if shape_idx == instruction_idx == 0:
        #     print(num_rocks_rest - prev_rocks_rest)
        #     print(len(chamber) - prev_height)
        #     print(num_shape_cycles)
        #     print(num_instr_cycles)
        #     num_shape_cycles = 0
        #     num_instr_cycles = 0
        #     prev_rocks_rest = num_rocks_rest
        #     prev_height = len(chamber)
        #     print()

        if new_rock:
            add_rows(chamber, 3)
            new_rock = False
            shape = shapes[shape_idx]
            shape_idx += 1
            shape_idx = shape_idx % len(shapes)
            if shape_idx == 0:
                num_shape_cycles += 1
            shape_cols = len(shape.split('\n')[0])
            shape_rows = shape.count('\n') + 1
            add_rows(chamber, shape_rows)
            shape_coords = [2, 0]

            print_chamber(chamber, shape, shape_coords)

        instruction = instructions[instruction_idx]
        instruction_idx += 1
        instruction_idx = instruction_idx % len(instructions)
        if instruction_idx == 0:
            num_instr_cycles += 1

        # Apply instruction
        if instruction == '<' and shape_coords[0] > 0 and not detect_collision(chamber, shape, (shape_coords[0] - 1, shape_coords[1])):
            shape_coords[0] -= 1
        elif instruction == '>' and shape_coords[0] + shape_cols < CHAMBER_WIDTH and not detect_collision(chamber, shape, (shape_coords[0] + 1, shape_coords[1])):
            shape_coords[0] += 1
        
        print_chamber(chamber, shape, shape_coords)

        if not can_fall_down(chamber, shape, shape_coords):
            rest_rock(chamber, shape, shape_coords)
            num_rocks_rest += 1
            # print_chamber(chamber, shape, shape_coords)
            row_idx = len(chamber) - 1
            while chamber[row_idx].count('.') == CHAMBER_WIDTH:
                chamber.pop()
                row_idx -= 1
            # add_rows(chamber, 3)
            shape = None
            new_rock = True
        else:
            shape_coords[1] += 1
        print_chamber(chamber, shape, shape_coords)
    
    print(len(chamber))


def part_two(instructions):
    """
    Algorithm: find repeating cycles at points where shape_idx and instruction_idx both start over.
    Usually, after a few cycles there's a repeating pattern.
    Find this number.
    Run the simulation to this point.
    Save the states (chamber, shape, shape_coords...etc.)
    Find how many repetitions are until the desired number of rocks.
    Multiply the values.
    Restore the saved state and execute the simulation to the rest.
    """
    chamber = []
    new_rock = True
    
    shape_idx = 0
    instruction_idx = 0
    num_rocks_rest = 0
    prev_rocks_rest = 0
    prev_height = 0
    num_shape_cycles = 0
    num_instr_cycles = 0

    # Number of rocks have rested until the repetition stabilized
    # first_alignment_num_rocks = 14 + 15 + 20 + 15
    first_alignment_num_rocks = 1739 + 1745
    
    # What's the height when the repetition stabilized
    # first_alignment_height = 28 + 28 + 26 + 27
    first_alignment_height = 2759 + 2767

    # The number when the repetition stabilizes
    # first_alignments_to_run = 4
    first_alignments_to_run = 2
    num_alignments = 0

    # Saved states
    saved_new_rock = None
    saved_chamber = None
    saved_shape = None
    saved_shape_coords = None

    while num_alignments < first_alignments_to_run:
        if shape_idx == instruction_idx == 0 and num_rocks_rest > 0:
            # print(num_rocks_rest - prev_rocks_rest)
            # print(len(chamber) - prev_height)
            # print(num_shape_cycles)
            # print(num_instr_cycles)
            # print()
            num_shape_cycles = 0
            num_instr_cycles = 0
            prev_rocks_rest = num_rocks_rest
            prev_height = len(chamber)
            num_alignments += 1
            if num_alignments == first_alignments_to_run:
                saved_new_rock = new_rock
                saved_chamber = deepcopy(chamber)
                saved_shape = shape
                saved_shape_coords = shape_coords
                break

        if new_rock:
            add_rows(chamber, 3)
            new_rock = False
            shape = shapes[shape_idx]
            shape_idx += 1
            shape_idx = shape_idx % len(shapes)
            if shape_idx == 0:
                num_shape_cycles += 1
            shape_cols = len(shape.split('\n')[0])
            shape_rows = shape.count('\n') + 1
            add_rows(chamber, shape_rows)
            shape_coords = [2, 0]

            print_chamber(chamber, shape, shape_coords)

        instruction = instructions[instruction_idx]
        instruction_idx += 1
        instruction_idx = instruction_idx % len(instructions)
        if instruction_idx == 0:
            num_instr_cycles += 1

        # Apply instruction
        if instruction == '<' and shape_coords[0] > 0 and not detect_collision(chamber, shape, (shape_coords[0] - 1, shape_coords[1])):
            shape_coords[0] -= 1
        elif instruction == '>' and shape_coords[0] + shape_cols < CHAMBER_WIDTH and not detect_collision(chamber, shape, (shape_coords[0] + 1, shape_coords[1])):
            shape_coords[0] += 1
        
        print_chamber(chamber, shape, shape_coords)

        if not can_fall_down(chamber, shape, shape_coords):
            rest_rock(chamber, shape, shape_coords)
            num_rocks_rest += 1
            row_idx = len(chamber) - 1
            while chamber[row_idx].count('.') == CHAMBER_WIDTH:
                chamber.pop()
                row_idx -= 1
            shape = None
            new_rock = True
        else:
            shape_coords[1] += 1
        print_chamber(chamber, shape, shape_coords)

    # Number of repetitions needed
    # num_of_alignments = 56 - 1
    # num_of_alignments = 28_571_428_570 - 1
    num_of_alignments = 573_065_901 - 1
    
    # How many rocks fall during a repetition
    # rock_per_alignment = 20 + 15
    rock_per_alignment = 1745
    
    # How the height increases after a repetition
    # height_per_alignment = 27 + 26
    height_per_alignment = 2767

    # How many rocks we have before the finishing simulation
    num_rocks_rest = first_alignment_num_rocks + num_of_alignments * rock_per_alignment

    # Restore state
    new_rock = saved_new_rock
    chamber = deepcopy(saved_chamber)
    shape = saved_shape
    shape_coords = saved_shape_coords

    while num_rocks_rest < 1_000_000_000_000:
    # while num_rocks_rest < 2022:
        if new_rock:
            add_rows(chamber, 3)
            new_rock = False
            shape = shapes[shape_idx]
            shape_idx += 1
            shape_idx = shape_idx % len(shapes)
            if shape_idx == 0:
                num_shape_cycles += 1
            shape_cols = len(shape.split('\n')[0])
            shape_rows = shape.count('\n') + 1
            add_rows(chamber, shape_rows)
            shape_coords = [2, 0]

            print_chamber(chamber, shape, shape_coords)

        instruction = instructions[instruction_idx]
        instruction_idx += 1
        instruction_idx = instruction_idx % len(instructions)
        if instruction_idx == 0:
            num_instr_cycles += 1

        # Apply instruction
        if instruction == '<' and shape_coords[0] > 0 and not detect_collision(chamber, shape, (shape_coords[0] - 1, shape_coords[1])):
            shape_coords[0] -= 1
        elif instruction == '>' and shape_coords[0] + shape_cols < CHAMBER_WIDTH and not detect_collision(chamber, shape, (shape_coords[0] + 1, shape_coords[1])):
            shape_coords[0] += 1
        
        print_chamber(chamber, shape, shape_coords)

        if not can_fall_down(chamber, shape, shape_coords):
            rest_rock(chamber, shape, shape_coords)
            num_rocks_rest += 1
            row_idx = len(chamber) - 1
            while chamber[row_idx].count('.') == CHAMBER_WIDTH:
                chamber.pop()
                row_idx -= 1
            shape = None
            new_rock = True
        else:
            shape_coords[1] += 1
        print_chamber(chamber, shape, shape_coords)

    print(first_alignment_height + num_of_alignments * height_per_alignment + len(chamber) - len(saved_chamber))


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.read()
    part_one(data.strip())
    part_two(data.strip())
