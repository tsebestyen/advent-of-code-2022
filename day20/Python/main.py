from llist import sllist


def swap_nodes(l: sllist, start_idx, num):
    if num == 0:
        return
    current_idx = start_idx
    for i in range(abs(num)):
        d = 1 if num > 0 else -1
        rotation = False
        t_idx = current_idx + d
        if current_idx == 1 and num < 0:
            t_idx = len(l) - 1 - 1
            rotation = True
        if current_idx < 0:
            current_idx = len(l) - 1
            t_idx = current_idx + d
        if current_idx == len(l) - 1 - 1 and num > 0:
            t_idx = 0
            rotation = True

        s = l.nodeat(current_idx)
        t = l.nodeat(t_idx)
        l.remove(s)

        if rotation and num < 0:
            l.append(s)
            current_idx = len(l) - 1
        elif rotation and num > 0:
            l.insertbefore((s.value[0], True), t)
            current_idx = 0
        if not rotation:
            if num < 0:
                l.insertbefore((s.value[0], True), t)
            else:
                l.insertafter((s.value[0], True), t)
            current_idx += d


def part_one(data):
    initial_data = data[:]
    l = sllist()
    for d in data:
        l.append((d, False))
    for num in initial_data:
        if num == 0:
            continue
        for n_idx, n in enumerate(l):
            if n[0] == num and n[1] == False:
                break
        n = l.nodeat(n_idx)
        
        swap_nodes(l, n_idx, n.value[0])
        
        if len(l) < 10:
            for n in l:
                print(n[0], end=' ')
            print()
    for zero_idx, n in enumerate(l):
        if n[0] == 0:
            break
    pos1 = (zero_idx + 1000) % len(initial_data)
    pos2 = (zero_idx + 2000) % len(initial_data)
    pos3 = (zero_idx + 3000) % len(initial_data)
    n1 = l.nodeat(pos1).value[0]
    n2 = l.nodeat(pos2).value[0]
    n3 = l.nodeat(pos3).value[0]

    print(n1 + n2 + n3)


def part_two(data):
    pass


if __name__ == '__main__':
    # with open('test_input') as f:
    with open('input') as f:
        data = f.readlines()

    data = [int(d) for d in data]
    part_one(data)
    part_two(data)
